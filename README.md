# Monaco 2018 race CLI report

This is a web app for reporting results of Monaco 2018 Racing.

## Build guide

### For development

Run these commands in root directory of the project:

Using docker-compose:

```console
cp .env.dev.example .env &&
docker-compose up --build -d
```

Using docker with compose plugin:

```console
cp .env.dev.example .env &&
docker compose up --build -d
```
