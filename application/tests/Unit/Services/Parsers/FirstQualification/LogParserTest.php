<?php

declare(strict_types=1);

namespace Tests\Unit\Services\Parsers\FirstQualification;

use App\Services\Parsers\FirstQualification\LogParser;
use App\Services\Parsers\FirstQualification\RecordParsers\AbbreviationParser;
use App\Services\Parsers\FirstQualification\RecordParsers\EndParser;
use App\Services\Parsers\FirstQualification\RecordParsers\StartParser;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\TestCase;

class LogParserTest extends TestCase
{
    protected LogParser $parser;
    protected string $tempFile;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tempFile = tempnam(sys_get_temp_dir(), 'test_');

        Storage::shouldReceive('directoryExists')
            ->once()
            ->andReturn(true);

        $this->parser = new LogParser();
    }


    /**
     * @test
     */
    public function start_log_file_parsing(): void
    {
        $testData = 'DRR2018-05-24_12:14:12.054';
        file_put_contents($this->tempFile, $testData);

        Storage::shouldReceive('fileExists')
            ->once()
            ->andReturn(true);
        Storage::shouldReceive('get')
            ->once()
            ->andReturn(file_get_contents($this->tempFile));

        $expectedData = [
            'abbreviation' => 'DRR',
            'date' => '2018-05-24',
            'time' => '12:14:12.054'
        ];

        $this->assertTrue(in_array($expectedData, $this->parser->getData(new StartParser())));
    }

    /**
     * @test
     */
    public function end_log_file_parsing(): void
    {
        $testData = 'CSR2018-05-24_13:04:28.095';
        file_put_contents($this->tempFile, $testData);

        Storage::shouldReceive('fileExists')
            ->once()
            ->andReturn(true);
        Storage::shouldReceive('get')
            ->once()
            ->andReturn(file_get_contents($this->tempFile));

        $expectedData = [
            'abbreviation' => 'CSR',
            'date' => '2018-05-24',
            'time' => '13:04:28.095'
        ];

        $this->assertTrue(in_array($expectedData, $this->parser->getData(new EndParser())));
    }

    /**
     * @test
     */
    public function abbreviation_log_file_parsing(): void
    {
        $testData = 'PGS_Pierre Gasly_SCUDERIA TORO ROSSO HONDA';
        file_put_contents($this->tempFile, $testData);

        Storage::shouldReceive('fileExists')
            ->once()
            ->andReturn(true);
        Storage::shouldReceive('get')
            ->once()
            ->andReturn(file_get_contents($this->tempFile));

        $expectedData = [
            'abbreviation' => 'PGS',
            'driver' => 'Pierre Gasly',
            'car' => 'SCUDERIA TORO ROSSO HONDA'
        ];

        $this->assertTrue(in_array($expectedData, $this->parser->getData(new AbbreviationParser())));
    }


    protected function tearDown(): void
    {
        parent::tearDown();

        unlink($this->tempFile);
    }
}
