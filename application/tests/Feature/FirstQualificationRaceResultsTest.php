<?php

declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FirstQualificationRaceResultsTest extends TestCase
{
    use RefreshDatabase;

    private array $expectedData;

    protected function setUp(): void
    {
        parent::setUp();

        $this->expectedData = [
            'driver' => 'Charles Leclerc',
            'car' => 'SAUBER FERRARI',
            'abbreviation' => 'CLS',
            'date' => '2018-05-24',
            'interval' => '01:01:12.829'
        ];

        $this->artisan('db:import-base-data');
    }

    /**
     * @test
     */
    public function welcome_page_loading(): void
    {
        $response = $this->get('/');

        $response->assertSee('Welcome!');
    }

    /**
     * @test
     */
    public function report_page_loading_and_has_results(): void
    {
        $response = $this->get('/report');

        $response->assertStatus(200);
        $response->assertViewHas('results', function ($results) {
            return in_array($this->expectedData, $results);
        });
    }

    /**
     * @test
     */
    public function drivers_page_loading_and_has_drivers(): void
    {
        $response = $this->get('/report/drivers');

        $response->assertViewHas('drivers', function ($drivers) {
            return in_array($this->expectedData, $drivers);
        });
    }

    /**
     * @test
     */
    public function driver_page_loading_and_has_information(): void
    {
        $response = $this->get('/report/drivers/' . $this->expectedData['abbreviation']);

        $response->assertViewHas('driver', function ($driver) {
            return $driver === $this->expectedData;
        });
    }

    /**
     * @test
     */
    public function error_page_loading_on_non_existing_driver(): void
    {
        $driverCode = 'WWW';
        $message = '404 | Driver not found';

        $response = $this->get('/report/drivers/' . $driverCode);

        $response->assertSee($message);
    }
}
