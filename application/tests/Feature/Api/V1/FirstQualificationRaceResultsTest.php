<?php

namespace Tests\Feature\Api\V1;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FirstQualificationRaceResultsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:import-base-data');
    }


    /**
     * @test
     */
    public function getting_json_race_results(): void
    {
        $response = $this->getJson('/api/v1/report?format=json');

        $expectedData = [
            'abbreviation' => 'CLS',
            'driver' => 'Charles Leclerc',
            'car' => 'SAUBER FERRARI',
            'date' => '2018-05-24',
            'interval' => '01:01:12.829'
        ];

        $response->assertStatus(200);
        $response->assertJsonFragment($expectedData);
    }

    /**
     * @test
     */
    public function getting_xml_race_results(): void
    {
        $response = $this->getJson('/api/v1/report?format=xml');

        $expectedData = '<result><driver>Esteban Ocon</driver><car>FORCE INDIA MERCEDES</car><abbreviation>EOF</abbreviation><date>2018-05-24</date><interval>00:54:13.028</interval></result>';

        $content = $response->getContent();

        $response->assertStatus(200);
        $this->assertStringContainsString($expectedData, $content);
    }
}
