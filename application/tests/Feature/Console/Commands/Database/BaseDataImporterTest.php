<?php

namespace Tests\Feature\Console\Commands\Database;

use App\Models\Driver;
use App\Models\Lap;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BaseDataImporterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function table_seeding(): void
    {
        $this->artisan('db:import-base-data')->assertSuccessful();

        $driver = Driver::find(1);
        $lap = Lap::find(1);

        $this->assertModelExists($driver);
        $this->assertModelExists($lap);
    }
}
