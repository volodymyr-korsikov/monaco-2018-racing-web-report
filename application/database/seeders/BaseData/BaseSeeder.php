<?php

namespace Database\Seeders\BaseData;

use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->call([
            DriversSeeder::class,
            LapsSeeder::class
        ]);
    }
}
