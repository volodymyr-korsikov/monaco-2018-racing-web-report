<?php

namespace Database\Seeders\BaseData;

use App\Models\Driver;
use App\Services\DataProviders\FirstQualification\BaseDataForTables\DriversProvider;
use Illuminate\Database\Seeder;

class DriversSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(DriversProvider $provider): void
    {
        Driver::truncate();

        $drivers = $provider->getTableData();

        foreach ($drivers as $driver) {
            Driver::create($driver);
        }
    }
}
