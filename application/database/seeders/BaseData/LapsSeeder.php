<?php

namespace Database\Seeders\BaseData;

use App\Models\Lap;
use App\Services\DataProviders\FirstQualification\BaseDataForTables\LapsProvider;
use Illuminate\Database\Seeder;

class LapsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(LapsProvider $provider): void
    {
        Lap::truncate();

        $laps = $provider->getTableData();

        foreach ($laps as $lap) {
            Lap::create($lap);
        }
    }
}
