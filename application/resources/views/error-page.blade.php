<x-layout
    title="Error">
    <h2 class="text-center text-white bg-danger">
        {{ $code . ' | ' . $message }}
    </h2>
</x-layout>
