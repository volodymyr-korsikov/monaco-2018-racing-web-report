<x-layout
    title="API Docs">
    <div
        class="row justify-content-center mt-2 mb-4">
        <div
            class="col col-8">
            <ol class="list-group list-group-numbered border border-light">
                <li class="list-group-item"
                    style="background: transparent; color: white">
                    <a href="{{ url('/api/documentation') }}"
                       class="link-underline-primary">Swagger</a>
                </li>
                <li class="list-group-item"
                    style="background: transparent; color: white">
                    <a href="{{ url('/docs/api-docs.json') }}"
                       class="link-underline-primary">Swagger JSON</a>
                </li>
            </ol>
        </div>
    </div>
</x-layout>
