<x-layout
    title="Drivers">
    <div
        class="row justify-content-center mt-2 mb-4">
        <div
            class="col col-8">
            <x-order-dropdown/>
            <ol class="list-group list-group-numbered border border-light">
                @foreach($drivers as $driver)
                    <li class="list-group-item"
                        style="background: transparent; color: white">
                        {{ $driver['driver'] . ', ' }}
                        <a href="{{ url("/report/drivers/{$driver['abbreviation']}") }}"
                           class="link-underline-primary">{{ $driver['abbreviation'] }}</a>
                    </li>
                @endforeach
            </ol>
        </div>
    </div>
</x-layout>
