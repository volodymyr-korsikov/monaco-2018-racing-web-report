<x-layout
    title="Login">
    <div
        class="row justify-content-center mt-2 mb-4">
        <div
            class="col col-3">
            <form
                action="{{ url('/login_error') }}"
                method="get">
                <div
                    class="mb-3">
                    <label
                        for="email1"
                        class="form-label">Email address</label>
                    <input
                        type="email"
                        class="form-control"
                        id="email1">
                </div>
                <div
                    class="mb-3">
                    <label
                        for="password"
                        class="form-label">Password</label>
                    <input
                        type="password"
                        class="form-control"
                        id="password">
                </div>
                <button
                    type="submit"
                    class="btn btn-primary">Submit
                </button>
            </form>
        </div>
    </div>
</x-layout>
