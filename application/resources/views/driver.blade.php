<x-layout
    title="{{ $driver['abbreviation'] }}">
    <div
        class="row justify-content-center mt-2 mb-4">
        <div
            class="col col-8">
            <table
                class="table text-white table-bordered">
                <thead>
                <tr>
                    <th scope="col">
                        Driver
                    </th>
                    <th scope="col">
                        Code
                    </th>
                    <th scope="col">
                        Car
                    </th>
                    <th scope="col">
                        Date
                    </th>
                    <th scope="col">
                        Time
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $driver['driver'] }}</td>
                    <td>{{ $driver['abbreviation'] }}</td>
                    <td>{{ $driver['car'] }}</td>
                    <td>{{ $driver['date'] }}</td>
                    <td>{{ $driver['interval'] }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</x-layout>
