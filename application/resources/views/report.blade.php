<x-layout
    title="Report">
    <div
        class="row justify-content-center mt-2 mb-4">
        <div
            class="col col-8">
            <x-order-dropdown/>
            <table
                class="table text-white table-bordered">
                <thead>
                <tr>
                    <th scope="col">
                        #
                    </th>
                    <th scope="col">
                        Driver
                    </th>
                    <th scope="col">
                        Car
                    </th>
                    <th scope="col">
                        Time
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $id => $result)
                    <tr>
                        <th scope="row">{{ $id + 1 }}
                            .
                        </th>
                        <td>{{ $result['driver'] }}</td>
                        <td>{{ $result['car'] }}</td>
                        <td>{{ $result['interval'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-layout>
