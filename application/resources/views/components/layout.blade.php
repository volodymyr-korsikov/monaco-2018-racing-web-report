<!doctype html>
<html
    lang="en">
<head>
    <meta
        charset="UTF-8">
    <meta
        name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta
        http-equiv="X-UA-Compatible"
        content="ie=edge">
    <title>{{ $title }}</title>
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
</head>
<body
    class="text-white"
    style="background-color: darkgreen;">

<div
    class="container">
    <div
        class="row mt-3">
        <h1 class="text-center">
            Q1
            of
            Monaco
            2018
            Racing
        </h1>
    </div>
    <div
        class="row justify-content-center border-top border-bottom">
        <div
            class="col col-6">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ url('/')  }}">#</a>
                </li>
                <li class="nav-item">
                    <b>
                        <a class="nav-link active"
                           aria-current="page"
                           href="{{ url()->current() }}">{{ $title }}</a>
                    </b>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ url('/report')  }}">Report</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ url('/report/drivers') }}">Drivers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ url('/documentation') }}">API Docs</a>
                </li>
            </ul>
        </div>
    </div>
    {{ $slot }}

</div>

<script
    src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
</body>
</html>
