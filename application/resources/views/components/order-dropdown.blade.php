<div
    class="my-3">
    <div
        class="dropdown">
        <button
            class="btn border-light text-light dropdown-toggle"
            type="button"
            id="dropdownMenuButton1"
            data-bs-toggle="dropdown"
            aria-expanded="false">
            Order
        </button>
        <ul class="dropdown-menu"
            aria-labelledby="dropdownMenuButton1">
            <li>
                <a class="dropdown-item"
                   href="{{ url()->current() . '?order=asc' }}">Ascending</a>
            </li>
            <li>
                <a class="dropdown-item"
                   href="{{ url()->current() . '?order=desc' }}">Descending</a>
            </li>
        </ul>
    </div>
</div>
