<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\RaceResults;

use App\Http\Controllers\Controller;
use App\Services\Converters\ArrayToStringConverters\Converter;
use App\Services\Converters\ArrayToStringConverters\ConverterCreator;
use App\Services\DataProviders\FirstQualification\ResultsProvider;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use OpenApi\Attributes as OA;

#[OA\Info(
    version: '1',
    title: 'Q1 Race Results'
)]
class FirstQualificationController extends Controller
{
    protected Converter $converter;
    protected string $format;

    public function __construct(
        protected ResultsProvider $provider,
    )
    {
    }


    #[OA\Get(
        path: "/api/v1/report",
        description: 'Returns Q1 results in specified format (default: json)',
        parameters: [
            new OA\Parameter(name: 'format', in: 'query', allowEmptyValue: true, example: 'xml')
        ],
        responses: [
            new OA\Response(response: '200', description: 'Results')
        ]
    )]
    public function index(Request $request): Response
    {
        $format = $request->input('format') ?? 'json';

        $this->converter = ConverterCreator::create($format);
        $results = $this->provider->getRaceResults();

        return response($this->converter->convert($results, 'results'))->header('Content-Type', 'text/' . $format);
    }
}
