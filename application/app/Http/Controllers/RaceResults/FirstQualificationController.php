<?php

declare(strict_types=1);

namespace App\Http\Controllers\RaceResults;

use App\Http\Controllers\Controller;
use App\Services\DataProviders\FirstQualification\ResultsProvider;
use Illuminate\Http\Request;
use Illuminate\View\View;

class FirstQualificationController extends Controller
{
    protected string $order;

    public function __construct(
        protected ResultsProvider $provider,
        protected Request         $request
    )
    {
        $this->order = $request->input('order') ?? 'asc';
    }


    public function index(): View
    {
        $results = $this->provider->getRaceResults($this->order);
        return view('report', ['results' => $results]);
    }

    public function drivers(): View
    {
        $drivers = $this->provider->getRaceResults($this->order);
        return view('drivers', ['drivers' => $drivers]);
    }

    public function show(string $code): View
    {
        if (!$this->provider->driverExists($code)) {
            $errorInformation = [
                'code' => 404,
                'message' => 'Driver not found'
            ];

            return view('error-page', $errorInformation);
        }

        $driver = $this->provider->getDriverResult($code);
        return view('driver', ['driver' => $driver]);
    }
}
