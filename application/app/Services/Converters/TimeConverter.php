<?php

declare(strict_types=1);

namespace App\Services\Converters;

class TimeConverter
{
    public static function toTime(int $milliseconds): string
    {
        $millisecondsInSeconds = floor($milliseconds / 1000);

        $hours = floor($millisecondsInSeconds / 3600);
        $minutes = floor(($millisecondsInSeconds % 3600) / 60);
        $seconds = $millisecondsInSeconds % 60;
        $milliseconds = $milliseconds % 1000;
        
        $time = sprintf("%02d:%02d:%02d.%03d", $hours, $minutes, $seconds, $milliseconds);

        return $time;
    }

    public static function toMilliseconds(string $time): int
    {
        list($time, $milliseconds) = explode('.', $time);
        list($hours, $minutes, $seconds) = explode(':', $time);
        $milliseconds += ((int)$hours * 3600 + (int)$minutes * 60 + (int)$seconds) * 1000;

        return $milliseconds;
    }
}
