<?php

declare(strict_types=1);

namespace App\Services\Converters\ArrayToStringConverters;

interface Converter
{
    public function convert(array $data, string $rootName): string;
}
