<?php

declare(strict_types=1);

namespace App\Services\Converters\ArrayToStringConverters;

/**
 * ConverterCreator provides function to create different converters that registered within it.
 */
class ConverterCreator
{
    protected static array $converters = [
        'json' => ConverterToJSON::class,
        'xml' => ConverterToXML::class
    ];


    /**
     * @return Converter - interface for array to string converters.
     */
    public static function create(string $format): Converter
    {
        $converters = self::$converters;
        foreach ($converters as $dataType => $converter) {
            if ($dataType === $format) {
                return new $converter();
            }
        }

        $defaultConverter = $converters['json'];
        return new $defaultConverter();
    }
}
