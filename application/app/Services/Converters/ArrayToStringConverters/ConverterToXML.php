<?php

declare(strict_types=1);

namespace App\Services\Converters\ArrayToStringConverters;

use Illuminate\Support\Str;
use SimpleXMLElement;

class ConverterToXML implements Converter
{
    public function convert(array $data, string $rootName): string
    {
        $xml = new SimpleXMLElement("<{$rootName}></{$rootName}>");

        return $this->generateXML($data, $xml)->asXML();
    }


    private function generateXML(array $data, SimpleXMLElement &$xml): SimpleXMLElement
    {
        foreach ($data as $key => $value) {
            if (is_int($key)) {
                $key = Str::singular($xml->getName());
            }

            if (is_array($value)) {
                $root = $xml->addChild($key);
                $this->generateXML($value, $root);
            } else {
                $xml->addChild($key, (string)$value);
            }
        }

        return $xml;
    }
}
