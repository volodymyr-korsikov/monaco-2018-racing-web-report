<?php

declare(strict_types=1);

namespace App\Services\Converters\ArrayToStringConverters;

class ConverterToJSON implements Converter
{

    public function convert(array $data, string $rootName): string
    {
        $data = [$rootName => $data];

        return json_encode($data);
    }
}
