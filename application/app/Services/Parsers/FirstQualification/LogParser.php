<?php

namespace App\Services\Parsers\FirstQualification;

use App\Exceptions\CustomException;
use App\Services\Parsers\FirstQualification\RecordParsers\RecordParser;
use Illuminate\Support\Facades\Storage;

/**
 * LogParser provides parsing of log files with Q1 race results.
 *
 * It provides checks for log files in the storage as well as their parsing by rules defined in implementation(s) of RecordParser interface.
 */
class LogParser
{
    private const DATA_FOLDER = 'data/first_qualification/';
    private string $dataFolder;

    public function __construct()
    {
        $this->setDataFolder(self::DATA_FOLDER);
    }

    private function setDataFolder(string $dataFolder): void
    {
        if (!Storage::directoryExists($dataFolder)) {
            throw new CustomException("Folder '{$dataFolder}' does not exist");
        }

        $this->dataFolder = $dataFolder;
    }


    /** Returns array of parsed records by rules defined in "record parsers". */
    public function getData(RecordParser $recordParser): array
    {
        if (!Storage::fileExists($this->dataFolder . $recordParser->getFile())) {
            throw new CustomException("File '{$recordParser->getFile()}' is absent in '{$this->dataFolder}'");
        }

        $records = explode(
            PHP_EOL,
            trim(Storage::get($this->dataFolder . $recordParser->getFile()))
        );

        $data = [];
        foreach ($records as $record) {
            $data[] = $recordParser->parse($record);
        }

        return $data;
    }
}
