<?php

declare(strict_types=1);

namespace App\Services\Parsers\FirstQualification\RecordParsers;

class AbbreviationParser implements RecordParser
{
    public function getFile(): string
    {
        return 'abbreviations.txt';
    }

    public function parse(string $record): array
    {
        $parts = explode('_', $record);

        return [
            'abbreviation' => $parts[0],
            'driver' => $parts[1],
            'car' => $parts[2]
        ];
    }
}
