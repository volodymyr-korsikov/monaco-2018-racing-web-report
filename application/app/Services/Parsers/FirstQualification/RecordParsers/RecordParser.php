<?php

declare(strict_types=1);

namespace App\Services\Parsers\FirstQualification\RecordParsers;

interface RecordParser
{
    public function getFile(): string;
    public function parse(string $record): array;
}
