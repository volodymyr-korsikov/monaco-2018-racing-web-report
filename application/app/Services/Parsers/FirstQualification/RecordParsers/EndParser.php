<?php

declare(strict_types=1);

namespace App\Services\Parsers\FirstQualification\RecordParsers;

use App\Services\Parsers\FirstQualification\RecordParsers\RecordParser;

class EndParser implements RecordParser
{

    public function getFile(): string
    {
        return 'end.log';
    }

    public function parse(string $record): array
    {
        $parts = explode('_', $record);

        return [
            'abbreviation' => substr($parts[0], 0, 3),
            'date' => substr($parts[0], 3, strlen($parts[0]) - 3),
            'time' => $parts[1]
        ];
    }
}
