<?php

declare(strict_types=1);

namespace App\Services\DataProviders\FirstQualification\BaseDataForTables;

use App\Exceptions\CustomException;
use App\Services\Parsers\FirstQualification\LogParser;
use App\Services\Parsers\FirstQualification\RecordParsers\AbbreviationParser;
use App\Services\Parsers\FirstQualification\RecordParsers\EndParser;
use App\Services\Parsers\FirstQualification\RecordParsers\StartParser;
use DateTime;

abstract class AbstractProvider
{
    public function __construct(
        private LogParser $parser
    )
    {
    }


    abstract public function getTableData(): array;


    protected function getFormattedResults(): array
    {
        $abbreviationsExplanations = $this->parser->getData(new AbbreviationParser());
        $startOfLaps = $this->parser->getData(new StartParser());
        $endOfLaps = $this->parser->getData(new EndParser());

        $results = [];
        foreach ($abbreviationsExplanations as $abbreviationExplanation) {
            $abbreviation = $abbreviationExplanation['abbreviation'];
            $results[] = [
                'abbreviation' => $abbreviation,
                'driver' => $abbreviationExplanation['driver'],
                'car' => $abbreviationExplanation['car'],
                'date' => $this->getItemByAbbreviation($abbreviation, 'date', $startOfLaps),
                'interval' => $this->getInterval(
                    $this->getItemByAbbreviation($abbreviation, 'time', $startOfLaps),
                    $this->getItemByAbbreviation($abbreviation, 'time', $endOfLaps)
                )
            ];
        }

        return $results;
    }

    private function getItemByAbbreviation(string $abbreviation, string $item, array $records): string
    {
        foreach ($records as $record) {
            if ($record['abbreviation'] === $abbreviation) {
                return $record[$item];
            }
        }

        throw new CustomException("Item '{$item}' not found");
    }

    private function getInterval(string $start, string $end): string
    {
        $startTime = DateTime::createFromFormat('H:i:s.u', $start);
        $endTime = DateTime::createFromFormat('H:i:s.u', $end);

        $interval = $endTime->diff($startTime);

        return substr($interval->format('%H:%I:%S.%F'), 0, strlen($interval->format('%H:%I:%S.%F')) - 3);
    }
}
