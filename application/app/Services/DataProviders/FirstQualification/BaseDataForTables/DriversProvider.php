<?php

declare(strict_types=1);

namespace App\Services\DataProviders\FirstQualification\BaseDataForTables;

class DriversProvider extends AbstractProvider
{
    public function getTableData(): array
    {
        $results = $this->getFormattedResults();

        $data = [];
        foreach ($results as $id => $result) {
            $driver = explode(' ', $result['driver']);

            $data[$id] = [
                'name' => $driver[0],
                'surname' => $driver[1],
                'car' => $result['car'],
                'abbreviation' => $result['abbreviation']
            ];
        }

        return $data;
    }
}
