<?php

declare(strict_types=1);

namespace App\Services\DataProviders\FirstQualification\BaseDataForTables;

use App\Services\Converters\TimeConverter;

class LapsProvider extends AbstractProvider
{
    public function getTableData(): array
    {
        $results = $this->getFormattedResults();

        $data = [];
        foreach ($results as $id => $result) {
            $milliseconds = TimeConverter::toMilliseconds($result['interval']);

            $data[$id] = [
                'date' => $result['date'],
                'time' => $milliseconds,
                'driver_id' => $id + 1
            ];
        }

        return $data;
    }
}
