<?php

declare(strict_types=1);

namespace App\Services\DataProviders\FirstQualification;

use App\Models\Driver;
use App\Services\Converters\TimeConverter;
use Illuminate\Support\Facades\DB;

/**
 * ResultsProvider provides methods for getting race results from database.
 */
class ResultsProvider
{
    public function getRaceResults(string $order = 'asc'): array
    {
        $results = Driver::join('laps', 'drivers.id', '=', 'laps.driver_id')
            ->select(
                DB::raw("drivers.name || ' ' || drivers.surname as driver"),
                'drivers.car',
                'drivers.abbreviation',
                'laps.date',
                'laps.time as interval'
            )
            ->orderBy('time', $order)
            ->get();

        $results->each(function ($result) {
            $result->interval = TimeConverter::toTime($result->interval);
        });

        return $results->toArray();
    }

    public function getDriverResult(string $code): array
    {
        $driver = Driver::join('laps', 'drivers.id', '=', 'laps.driver_id')
            ->select(
                DB::raw("drivers.name || ' ' || drivers.surname as driver"),
                'drivers.car',
                'drivers.abbreviation',
                'laps.date',
                'laps.time as interval'
            )
            ->where('drivers.abbreviation', '=', $code)
            ->first();

        $driver->interval = TimeConverter::toTime($driver->interval);

        return $driver->toArray();
    }

    public function driverExists(string $code): bool
    {
        if (!Driver::where('abbreviation', '=', $code)->first()) {
            return false;
        }

        return true;
    }
}
