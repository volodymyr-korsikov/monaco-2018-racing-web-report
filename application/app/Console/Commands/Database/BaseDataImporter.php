<?php

declare(strict_types=1);

namespace App\Console\Commands\Database;

use Database\Seeders\BaseData\BaseSeeder;
use Illuminate\Console\Command;

class BaseDataImporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import-base-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports base data from log files into database';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->call('db:seed', [
            '--class' => BaseSeeder::class,
            '--force' => true
        ]);

        $this->line('Tables are populated with base data');
    }
}
