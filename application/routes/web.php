<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// TODO: Authentication/authorization logic is required
Route::get('/admin/dashboard', function () {
    return view('admin/dashboard');
})->name('home')->middleware('auth');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/login_error', function () {
    $errorInformation = [
        'code' => 403,
        'message' => 'Login not available'
    ];

    return view('error-page', $errorInformation);
});

Route::controller(App\Http\Controllers\RaceResults\FirstQualificationController::class)->group(function () {
    Route::get('/report', 'index');
    Route::get('/report/drivers', 'drivers');
    Route::get('/report/drivers/{code}', 'show')
        ->where('code', '[A-Z]{3}');
});

Route::get('/documentation', function () {
    return view('api-docs');
});
