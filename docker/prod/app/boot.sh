#!/bin/bash

# Execution inside app container
cp .env.prod.example .env
php artisan key:generate
php artisan cache:clear
php artisan migrate --force
php artisan db:import-base-data
php artisan config:cache
php artisan route:cache
php artisan view:cache

# Start of the server
php-fpm
