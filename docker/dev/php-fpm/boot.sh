#!/bin/bash

# Execution inside app container
cp .env.dev.example .env
composer install
chmod -R 777 storage/
php artisan cache:clear
php artisan key:generate
php artisan migrate
php artisan db:import-base-data

# Start of the server
php-fpm
